#!/bin/bash

set -e
#
# This script is meant for quick & easy install via:
# 'curl -L https://bitbucket.org/marcos_cano/test_repo/raw/master/test.sh |bash'



echo "INSTALL YOOSYNC"



command_exists() {
	command -v "$@" > /dev/null 2>&1
}

check_check(){
	echo "checking prerequesites..."
	#check if root
	# if [ "$(id -u)" != "0" ]; then
	#    echo "This script must be run as root" 1>&2
	#    exit 1
	# fi

	### 64 bits??
	case "$(uname -m)" in
		*64)
			;;
		*)
			cat >&2 <<-'EOF'
			Error: you are not using a 64bit platform.
			Docker currently only supports 64bit platforms.
			EOF
			exit 1
			;;
	esac

	#### supported PLATFORM??
	lsb_dist=''
	if command_exists lsb_release; then
		lsb_dist="$(lsb_release -si)"
	fi
	if [ -z "$lsb_dist" ] && [ -r /etc/lsb-release ]; then
		lsb_dist="$(. /etc/lsb-release && echo "$DISTRIB_ID")"
	fi
	if [ -z "$lsb_dist" ] && [ -r /etc/debian_version ]; then
		lsb_dist='debian'
	fi
	lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"
	case "$lsb_dist" in
		ubuntu|debian|linuxmint)
				echo "YES"
				;;
		*)
		echo "your platform is not supported yet, this installer has only been test in ubuntu/debian/linuxmint"
		;;
	esac


	#check if docker is installed
	if [  -z "$(which docker)"  ] ; then
		echo "docker is not installed"
		echo "please visit https://docs.docker.com/installation/" 
		exit 1
	fi

	if ! command_exists docker && ! command_exists lxc-docker; then
			cat >&2 <<-'EOF'
			Warning: "docker" or "lxc-docker" is not installed.
			please visit https://docs.docker.com/installation/ and install it.	
			EOF
	fi

	##check that git is installed
	if ! command_exists git;then
		cat >&2 <<-'EOF'
			Warning: "git"  is not installed.
			please install it.	
			EOF
	fi

}

check_check



install(){

	git clone git@bitbucket.org:marcos_cano/docker_yoosync.git

	cd docker_yoosync

	./build.yoosync.sh

}

install



